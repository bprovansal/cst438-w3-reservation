package cst438reservation.domain;

//import org.apache.logging.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener
public class ReservationEventHandler {
	private static final Logger log = LoggerFactory.getLogger(ReservationEventHandler.class);
	
	@RabbitHandler
	public void receive(String in) {
		System.out.println(in);
		log.info(" [ReservationEventHandler] Received '" +
				in + "'");
	}
}
